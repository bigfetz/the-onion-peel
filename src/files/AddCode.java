/**
 * Add Code is the major part of this application. This takes one java file and injects code into 
 * the file after every single method declaration. The injected code is a write out to file and a 
 * optional System.out.Print(methodName). This is used so that the user knows which method is being
 * used when and in which order.
 *  
 * @author Matthew Fetzer 
 */
package files;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JProgressBar;

public class AddCode
{
	final static String OUTPUT = " Has been accesed";
	//Regex for all legal forms of method declarations.
	final static String PATTERN = "\\s*(((public|protected|private)\\s+)|((final|static)\\s+))+.*?\\s+\\w+[\\w\\d]*\\(.*?\\)\\s*\\{";         
	//Regex used to find name of method.
	final static String METHOD_PATTERN = "\\w+[\\w\\d]*\\((.*?|\\))?";

	public void injectCode(File file, JProgressBar jpb) 
	{
		String current = "";
		String all = "";
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File("InjectedTemp/"+ file.getName()),true));
			Scanner scan = new Scanner(file);
			
			while(scan.hasNextLine())
			{
				current = scan.nextLine();
				all += current + "\n";
				if(current.matches(PATTERN))
				{	
					all += methodInject(current) + "\n";
				}
				//adds to progress bar
				jpb.setValue(jpb.getValue() + current.getBytes().length);
			}
			bw.write(all);
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String methodInject(String currentMethod)
	{
		Scanner token = new Scanner(currentMethod);
	    String tok;
	    
		while(token.hasNext())
		{
			tok = token.next();
			if(tok.matches(METHOD_PATTERN))
			{
				tok = tok.substring(0, tok.indexOf("("));
				return "BufferedWriter bw = new BufferedWriter(new FileWriter(new File(\"OutPut/data.txt\",true),true));\n 		bw.apend(\""+ tok  + "\");\n 		bw.close();";              
			}
		}
		return "";
	}


	
}

/**
 * This class is used to run the Ruby scripts to compile and arrange the files with ease. 
 * This currently is having a few second wait for the main class file to be executed.
 * 
 * @author Matthew Fetzer
 */
package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import javax.swing.JFileChooser;
import org.jruby.embed.ScriptingContainer;

public class runCode
{
	public runCode()
	{
        String[] path = new String[2];
        JFileChooser fileChooser = new JFileChooser();
        File f = new File(System.getProperty("user.dir"));
        fileChooser.setCurrentDirectory(f);
        int check = fileChooser.showOpenDialog(null);
        if (check == JFileChooser.APPROVE_OPTION){        
        	path[0] = fileChooser.getSelectedFile().getName(); //gets name of file        	
            path[1] = fileChooser.getSelectedFile().getPath();//gets path to file
            path[1] = path[1].replace(System.getProperty("user.dir"),"");//deletes path up to project
            path[1] = path[1].replace(path[0],"");//deletes name from path 
            path[0] = path[0].replace(".class", "");//deletes the .class extension from the name 
            path[1] = path[1].replaceFirst("/", "");//deletes first instance of "/" for Ruby code to work
        }try {
	        ScriptingContainer container = new ScriptingContainer();
	        container.setArgv(path);//sets ARGV
	        container.runScriptlet(new BufferedReader(new FileReader("driver.rb")), "driver.rb");
		} catch (FileNotFoundException e) {
			System.out.println("No File chosen or file does not exist");
		} catch (NullPointerException npe)
		{
		
		}
			 
	}
	
	

}

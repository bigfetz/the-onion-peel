/**
 * This application uses the View Controller style. This does not include the Model 
 * for minimal shifts in view and very simple UI control. 
 * 
 * @author Matthew Fetzer
 */

package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import files.AddCode;
import files.runCode;

public class Controller implements ActionListener
{
	//used to grab buttons
	private GUI g;
	
	public Controller(GUI g)
	{
		this.g = g;
		
	}
	
	public void actionPerformed(ActionEvent e) 
	{
	
		if(e.getSource() == this.g.injectCode)//if the inject button is pressed
		{
			/*
			 * This is the bulk of the inject code control. This takes the selected files and runs the addCode
			 * method on all selected files. 
			 * 
			 * Progress bar installed and updated for every line scanned. 
			 */
			
			System.out.println("inject code button pressed");//debugging check
			if(this.g.list.getSelectedValues().length > 0)//makes sure that any files are selected
			{
				
				//Takes the size of all the files and add them together for progress bar
				int currentSize = 0;
				for(int i = 0; i < this.g.list.getSelectedValues().length;i++)
				{
					currentSize += new File("files/" +(String) this.g.list.getSelectedValues()[i]).length();
			    }
				final int size1 = currentSize;
				
				//Threading needed so that GUI and the addCode method do not freeze the users computer. 
				Executor executor = java.util.concurrent.Executors.newSingleThreadExecutor();
				((ExecutorService) executor).submit(new Runnable() { public void run() {
				final AddCode ac = new AddCode();
				
				JFrame load = new JFrame("Loading");
				JPanel container = new JPanel();
				container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
				JProgressBar jpb = new JProgressBar(0,size1);
				JLabel fileName = new JLabel();

				load.setSize(270,70);
				
				jpb.setValue(0);
				jpb.setStringPainted(true);
				load.setVisible(true);
				container.add(jpb);
				container.add(fileName);
				
				load.add(container);
				for (int i = 0; i < g.list.getSelectedValues().length; i++) 
				{ 
						//running code injection method addCode
						fileName.setText("  Current File being injected " + g.list.getSelectedValues()[i]);
						ac.injectCode(new File("files/" + g.list.getSelectedValues()[i]) ,jpb);
				}
				jpb.setValue(g.list.getSelectedValues().length);	
				
				load.setVisible(false);
				load.dispose();
				}});
			JFrame done = new JFrame();
			done.add(new JLabel("Done"));
			done.setVisible(true);
			}
		}else if(e.getSource() == g.run)
		{
			
			new runCode();
		}
	}
}

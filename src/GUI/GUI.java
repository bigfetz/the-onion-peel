/**
 * GUI class meant to create the simple UI for the user. It is meant to not be 
 * costly but yet give the user enough help so they don't have much of a learning 
 * curve.
 * 
 * @author Matthew Fetzer
 */
package GUI;

import java.awt.GridLayout;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class GUI extends JFrame{

	private JPanel container;
	private JPanel buttonPanel;
	private JPanel filePanel;
	public JButton run;
	public JButton injectCode;
	public JList list;
	private JScrollPane scrollList;
	
	private Controller controller; 
	
	/**
	 * Initialize all objects.
	 * Calls Design
	 */
	public GUI()
	{
		this.controller = new Controller(this);
		this.container = new JPanel();
		this.buttonPanel = new JPanel(new GridLayout(2,1));
		this.filePanel = new JPanel();
		this.run = new JButton("Run Code");
		this.injectCode = new JButton("Inject Code");
		this.injectCode.addActionListener(this.controller);
		this.run.addActionListener(this.controller);
		design();
		add(this.container);
		
		setSize(220,200);
		setVisible(true);
	}
	
	/**
	 * Creates GUI and inputs lists into JList
	 */
	public void design()
	{
				
		this.buttonPanel.add(this.run);
		this.buttonPanel.add(this.injectCode);
		
		this.list = new JList(removeOSFiles(new File("files").list()));
		
		this.scrollList = new JScrollPane(this.list);
		this.filePanel.add(this.scrollList);
		this.filePanel.setSize(150, 300);
		
		this.container.add(this.buttonPanel);
		this.container.add(this.filePanel);
	}
	
	
	/**
	 * Removes all OS files put in automatically
	 * 
	 **/
	public String[] removeOSFiles(String[] s)
	{
		String[] files = new String[s.length];
		int index = 0;
		for(int i = 0; i < s.length; i++)
		{
			if(s[i].contains(".java"))
			{
				files[index] = s[i];
				index++;
			}	
		}	
		return files;
	}

	
}

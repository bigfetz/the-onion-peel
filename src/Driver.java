/**
 * Driver for Application. Calls the GUI method to start application
 * 
 * @author Matthew Fetzer
 */
import java.io.IOException;
import GUI.GUI;

public class Driver 
{
	public static void main( String[] args ) throws IOException
	{
		 new GUI();		
	}
}

#Author Matthew Fetzer

# method for getting all java files into array
def getArr(name)
i = 0
fileArr = Array.new
Dir.foreach(name) {
|x| 
if File.ftype(name +"/#{x}") == "directory" && x != "." && x!= ".." #. and .. check for OS X users
  fileComp(getArr(name +"/#{x}")) #recursive step for directory
elsif ".java" == File.extname(x)#if java file
  fileArr[i] = name + "/#{x}"; 
  i = i+1 
end
}
return fileArr
end

#method for taking array filled with java files and compiling them into .class files
def fileComp(arr)
  arr.map{
  |x|   
  system 'javac', x ; # compiles java files into .class
  }
end
#############################################################################################
#Start
fileComp(getArr("InjectedTemp/runTemp")) #creates the class files of all the java files
system 'java -cp ' +  ARGV[1] + ' ' + ARGV[0]



